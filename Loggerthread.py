from time import strftime, localtime
import logging
import sys
import os
import threading
import time

global log_items
log_items = []
global running
running = True
timestamp = ""
logging.getLogger('suds').setLevel(logging.DEBUG)
logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)


def setup_logger(logfile, level=logging.DEBUG):
    log = logging.getLogger(logfile)
    formatter = logging.Formatter('%(asctime)s - %(process)d - %(thread)d - \
        %(levelname)s - %(message)s', datefmt='%m%d%Y %H:%M:%S')
    filehandler = logging.FileHandler(logfile, mode='w')
    filehandler.setFormatter(formatter)
    log.setLevel(level)
    log.addHandler(filehandler)


def init():
    global timestamp
    global appdir
    global appname
    level = os.environ['PYAPPLOGLEVEL']
    appdir = os.environ['PYAPPDIR']
    appname = os.environ['PYAPPNAME']
    if not os.path.exists(appdir + "/log"):
        os.makedirs(appdir + "/log")
    stderrlog = open(appdir + "log/stderr.log", "a")
    sys.stderr = stderrlog
    timestamp = strftime("%Y%m%d%H%M", localtime())
    setup_logger(str(appdir) + '/log/' + appname + '.log.' + timestamp, level)
    log_info('Logger Initialised')


def exit():
    global running
    running = False


def log_writer():
    """Log Writer Thread"""
    global timestamp
    global appdir
    global appname
    global log_items
    logWriter = logging.getLogger(str(appdir) + '/log/' + appname + '.log.'
                                  + timestamp)
    global running
    while running:
        if len(log_items) > 0:
            log_pair = log_items.pop(0)
            if log_pair[0] == 'INFO':
                logWriter.info(log_pair[1])
            elif log_pair[0] == 'WARN':
                logWriter.warning(log_pair[1])
            elif log_pair[0] == 'ERROR':
                logWriter.error(log_pair[1])
            if appname + ' Logging Enabled' in log_pair[1]:
                logWriter.disabled = False
            elif appname + ' Logging Disabled' in log_pair[1]:
                logWriter.disabled = True
        else:
            time.sleep(1)

    items = len(log_items)
    i = 0
    while i < items:
        log_pair = log_items.pop(0)
        if log_pair[0] == 'INFO':
            logWriter.info(log_pair[1])
        elif log_pair[0] == 'WARN':
            logWriter.warning(log_pair[1])
        elif log_pair[0] == 'ERROR':
            logWriter.error(log_pair[1])
        i = i+1


def info(msg):
    log_info(msg)


def log_info(msg):
    global log_items
    frame = sys._getframe(1)
    msg = "[" + frame.f_code.co_filename + ":" \
        + str(frame.f_lineno) + " in " \
        + frame.f_code.co_name + " ] - " + msg
    log_items.append(('INFO', msg))


def log_warn(msg):
    global log_items
    frame = sys._getframe(1)
    msg = "[" + frame.f_code.co_filename + ":" \
        + str(frame.f_lineno) + " in " \
        + frame.f_code.co_name + " ] - " + msg
    log_items.append(('WARN', msg))


def log_error(msg):
    global log_items
    frame = sys._getframe(1)
    msg = "[" + frame.f_code.co_filename + ":" \
        + str(frame.f_lineno) + " in " \
        + frame.f_code.co_name + " ] - " + msg
    log_items.append(('ERROR', msg))


def enable_logging():
    global log_items
    global appname
    frame = sys._getframe(1)
    msg = "[" + frame.f_code.co_filename + ":" \
        + str(frame.f_lineno) + " in " \
        + frame.f_code.co_name + " ] - " + appname + " Logging Enabled"
    log_items.append(('INFO', msg))


def disable_logging():
    global log_items
    global appname
    frame = sys._getframe(1)
    msg = "[" + frame.f_code.co_filename + ":" \
        + str(frame.f_lineno) + " in " \
        + frame.f_code.co_name + " ] - " + appname + " Logging Disabled"
    log_items.append(('INFO', msg))


def getLogFile():
    global timestamp
    global appdir
    global appname
    return str(appdir) + '/log/' + appname + '.log.' + timestamp


init()
t = threading.Thread(target=log_writer)
t.start()
