export 3 variables

PYAPPDIR - the directory of your application
PYAPPNAME - the name of your application
PYAPPLOGLEVEL - the log level you require

Loggerthread will create a directory under PYAPPDIR call "log" for logging. Version 2 will allow this directory to be set as a variable.

Usage Example:
import Loggerthread as logger

logger.log_info("Loggerthread imported to application")
logger.exit()

** Very important to call exit on the logger thread or it will hang

Version 2 will add a SIGTERM handler
